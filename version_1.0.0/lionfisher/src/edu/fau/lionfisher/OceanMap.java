package edu.fau.lionfisher;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationService;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISFeatureLayer;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.LinearUnit;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.Unit;


public class OceanMap extends Activity {
	
	// create ArcGIS objects
	MapView mMapView;
	ArcGISTiledMapServiceLayer basemapOcean;
	ArcGISFeatureLayer flayer;
	GraphicsLayer gLayer;
	final static double ZOOM_RADIUS = 50;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_view);

 // Retrieve the map and initial extent from XML layout
 		mMapView = (MapView) findViewById(R.id.map);
 		/* create base map */
		basemapOcean = new ArcGISTiledMapServiceLayer(this.getResources()
				.getString(R.string.OCEAN_BASEMAP));
 		
 		// Add basemap to MapView
		mMapView.addLayer(basemapOcean);
 		// set visibility
		basemapOcean.setVisible(true);
		
 		// attribute ESRI logo to map
 		mMapView.setEsriLogoVisible(true);
   // enable map to wrap around date line
 		mMapView.enableWrapAround(true);

 		mMapView.setOnStatusChangedListener(new OnStatusChangedListener() {
 		
 			private static final long serialVersionUID = 1L;
 			
 		public void onStatusChanged(Object source, STATUS status) {
			if (source == mMapView && status == STATUS.INITIALIZED) {
				LocationService ls = mMapView.getLocationService();
				ls.setAutoPan(false);
				ls.setLocationListener(new LocationListener() {

					boolean locationChanged = false;

					// Zooms to the current location when first GPS fix
					// arrives.
					public void onLocationChanged(Location loc) {
						if (!locationChanged) {
							locationChanged = true;
							double locy = loc.getLatitude();
							double locx = loc.getLongitude();
							Point wgspoint = new Point(locx, locy);
							Point mapPoint = (Point) GeometryEngine
									.project(wgspoint,
											SpatialReference.create(4326),
											mMapView.getSpatialReference());

							Unit mapUnit = mMapView.getSpatialReference()
									.getUnit();
							double zoomWidth = Unit.convertUnits(
									ZOOM_RADIUS,
									Unit.create(LinearUnit.Code.MILE_US),
									mapUnit);
							Envelope zoomExtent = new Envelope(mapPoint,
									zoomWidth, zoomWidth);
							mMapView.setExtent(zoomExtent);

						}

					}

					public void onProviderDisabled(String arg0) {

					}

					public void onProviderEnabled(String arg0) {
					}

					public void onStatusChanged(String arg0, int arg1,
							Bundle arg2) {

					}
				});
				ls.start();

			}

		}
	});
 		
    }
    
    
	@Override 
	protected void onDestroy() { 
		super.onDestroy();
 }
	@Override
	protected void onPause() {
		super.onPause();
		mMapView.pause();
 }
	@Override 	protected void onResume() {
		super.onResume(); 
		mMapView.unpause();
	}

}