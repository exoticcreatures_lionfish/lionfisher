package edu.fau.lionfisher;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class ReportScreen extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_main);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			//Show the up button in the action bar
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	//This method calls the Report Form web site
	public void startForm (View view){
		Uri webpage = Uri.parse ("http://www.reef.org/programs/exotic/report");
		Intent webintent = new Intent (Intent.ACTION_VIEW, webpage);
		
		//verify this resolves to an app
		PackageManager packageManager = getPackageManager();
		List<ResolveInfo> activities = packageManager.queryIntentActivities (webintent, 0);
		boolean isIntentSafe = activities.size() > 0;
		
		//start the activity if its safe
		if (isIntentSafe){
		startActivity(webintent);	
		}
		
	}
	
	//This method calls the Report Form web site
	public void startWorldRecords (View view){
		Uri webpage = Uri.parse ("http://lionfish.co/biggest-lionfish/");
		Intent webintent = new Intent (Intent.ACTION_VIEW, webpage);
		
		//verify this resolves to an app
		PackageManager packageManager = getPackageManager();
		List<ResolveInfo> activities = packageManager.queryIntentActivities (webintent, 0);
		boolean isIntentSafe = activities.size() > 0;
		
		//start the activity if its safe
		if (isIntentSafe){
		startActivity(webintent);	
		}
		
	}	
	
	//This method calls the camera
	public void startCamera (View view){
		Intent cameraIntent = new Intent (android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivity(cameraIntent);
	} 
	
//	//This method calls the ReportScreen Activity to start running
//	public void startMap (View view){
//		Intent intent = new Intent (this, BaseMaps.class);
//		startActivity(intent);
//	} 
	
//	//This method calls the map view
//	public void startMap (View view){
//		Uri location = Uri.parse ("geo:26.372801,-80.098466?z=14");
//		Intent mapIntent = new Intent (Intent.ACTION_VIEW, location);
//		
//		//verify this resolves to an app
//		PackageManager packageManager = getPackageManager();
//		List<ResolveInfo> activities = packageManager.queryIntentActivities (mapIntent, 0);
//		boolean isIntentSafe = activities.size() > 0;
//		
//		if (isIntentSafe){
//		startActivity(mapIntent);	
//		}
//		
//	}
	
}
