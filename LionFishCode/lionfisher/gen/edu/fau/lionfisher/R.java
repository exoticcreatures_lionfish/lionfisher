/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package edu.fau.lionfisher;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int bg3=0x7f020000;
        public static final int ic_launcher=0x7f020001;
        public static final int report_button=0x7f020002;
        public static final int spacer=0x7f020003;
    }
    public static final class id {
        public static final int NatGeo=0x7f08000d;
        public static final int Ocean_Basemap=0x7f08000e;
        public static final int World_Street_Map=0x7f08000b;
        public static final int World_Topo=0x7f08000c;
        public static final int action_settings=0x7f08000f;
        public static final int button1=0x7f080004;
        public static final int button2=0x7f080005;
        public static final int button3=0x7f080007;
        public static final int button4=0x7f080009;
        public static final int button5=0x7f08000a;
        public static final int imageButton1=0x7f080001;
        public static final int map=0x7f080002;
        public static final int tableRow1=0x7f080003;
        public static final int tableRow2=0x7f080006;
        public static final int tableRow3=0x7f080008;
        public static final int textView1=0x7f080000;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int map_view=0x7f030001;
        public static final int report_main=0x7f030002;
    }
    public static final class menu {
        public static final int basemap_menu=0x7f070000;
        public static final int main=0x7f070001;
    }
    public static final class string {
        public static final int NatGeoMenu=0x7f05000e;
        public static final int OCEAN_BASEMAP=0x7f05000b;
        public static final int OceanMenu=0x7f05000f;
        /**  basemap menu items 
         */
        public static final int StreetMapMenu=0x7f05000c;
        public static final int TopoMenu=0x7f05000d;
        public static final int WORLD_NATGEO_MAP=0x7f05000a;
        /**  basemap urls 
         */
        public static final int WORLD_STREET_MAP=0x7f050008;
        public static final int WORLD_TOPO_MAP=0x7f050009;
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int camera_button=0x7f050007;
        public static final int hello_world=0x7f050002;
        public static final int map_button=0x7f050005;
        public static final int records_button=0x7f050006;
        public static final int report_button=0x7f050004;
        public static final int tag_button=0x7f050003;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
