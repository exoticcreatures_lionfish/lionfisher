package edu.fau.lionfisher;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class InfoScreen extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info_main);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			//Show the up button in the action bar
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
	
	//This method calls the Recipes web site
		public void startRecipes (View view){
			Uri webpage = Uri.parse ("http://www.lionfishhunters.org/Recipes.html");
			Intent webintent = new Intent (Intent.ACTION_VIEW, webpage);
			
			//verify this resolves to an app
			PackageManager packageManager = getPackageManager();
			List<ResolveInfo> activities = packageManager.queryIntentActivities (webintent, 0);
			boolean isIntentSafe = activities.size() > 0;
			
			//start the activity if its safe
			if (isIntentSafe){
			startActivity(webintent);	
			}
			
		}
		//This method calls the Info
		public void startInfo (View view){
			Uri webpage = Uri.parse ("http://en.wikipedia.org/wiki/Lionfish");
			Intent webintent = new Intent (Intent.ACTION_VIEW, webpage);
			
			//verify this resolves to an app
			PackageManager packageManager = getPackageManager();
			List<ResolveInfo> activities = packageManager.queryIntentActivities (webintent, 0);
			boolean isIntentSafe = activities.size() > 0;
			
			//start the activity if its safe
			if (isIntentSafe){
			startActivity(webintent);	
			}
			
		}
		//This method calls the Report Form web site
		public void startHandle (View view){
			Uri webpage = Uri.parse ("http://chatter.explorercharts.com/?p=997");
			Intent webintent = new Intent (Intent.ACTION_VIEW, webpage);
			
			//verify this resolves to an app
			PackageManager packageManager = getPackageManager();
			List<ResolveInfo> activities = packageManager.queryIntentActivities (webintent, 0);
			boolean isIntentSafe = activities.size() > 0;
			
			//start the activity if its safe
			if (isIntentSafe){
			startActivity(webintent);	
			}
			
		}
}
