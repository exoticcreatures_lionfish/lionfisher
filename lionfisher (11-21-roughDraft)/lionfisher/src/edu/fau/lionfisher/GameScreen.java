package edu.fau.lionfisher;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

public class GameScreen extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_main);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			//Show the up button in the action bar
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
}
