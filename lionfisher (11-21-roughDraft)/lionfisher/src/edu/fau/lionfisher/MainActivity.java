package edu.fau.lionfisher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	//This method calls the ReportScreen Activity to start running
	public void startMap (View view)
	{
		Intent intent = new Intent (this, OceanMap.class);
		startActivity(intent);
	} 
	
	//This method calls the InfoScreen Activity to start running
	public void startInfo (View view)
	{
		Intent intent = new Intent (this, InfoScreen.class);
		startActivity(intent);
	}
	
	//This method calls the ReportScreen Activity to start running
	public void startGame (View view)
	{
		Intent intent = new Intent (this, GameScreen.class);
		startActivity(intent);
	} 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();
 }
	@Override 	
	protected void onResume() {
		super.onResume(); 
	}	
	
}
