package edu.fau.lionfisher;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationService;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISFeatureLayer;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.core.geometry.Envelope;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.LinearUnit;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.Unit;
import com.esri.core.map.CallbackListener;
import com.esri.core.map.FeatureEditResult;
import com.esri.core.map.FeatureTemplate;
import com.esri.core.map.Graphic;


public class OceanMap extends Activity {
	
	// create ArcGIS objects
	MapView mMapView;
	ArcGISFeatureLayer featureLayer;
	ArcGISTiledMapServiceLayer tileLayer;
	GraphicsLayer graphicsLayer;
	
//	//define the button to tag my location
//	Button tagButton = null;
	
	Location mySpot = null;
	
	private String featureServiceURL;

	ProgressDialog progress;
	final static double ZOOM_RADIUS = 50;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_view);

 // Retrieve the map and initial extent from XML layout
        mMapView = (MapView) findViewById(R.id.map);

        mMapView.enableWrapAround(true);

      //Display a toast telling the user what to do
		Toast toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
//		myListener.setType("POINT");
		toast.setText("Tap the pointer in the action bar above to save your current location on the map."); 						
		toast.show();
		
        
		// Get the feature service URL from values->strings.xml
		featureServiceURL = this.getResources().getString(
				R.string.featureServiceURL);

		// Add Tile layer to the MapView
		tileLayer = new ArcGISTiledMapServiceLayer(this.getResources()
				.getString(R.string.tileServiceURL));
		mMapView.addLayer(tileLayer);
		// Add Feature layer to the MapView
		featureLayer = new ArcGISFeatureLayer(featureServiceURL,
				ArcGISFeatureLayer.MODE.ONDEMAND);
		mMapView.addLayer(featureLayer);
		// Add Graphics layer to the MapView
		graphicsLayer = new GraphicsLayer();
		mMapView.addLayer(graphicsLayer);

 		mMapView.setOnStatusChangedListener(new OnStatusChangedListener() {
 		
 			private static final long serialVersionUID = 1L;
 			
 		public void onStatusChanged(Object source, STATUS status) {
			if (source == mMapView && status == STATUS.INITIALIZED) {
				LocationService ls = mMapView.getLocationService();
				ls.setAutoPan(false);
				ls.setLocationListener(new LocationListener() {

					boolean locationChanged = false;

					// Zooms to the current location when first GPS fix
					// arrives.
					public void onLocationChanged(Location loc) {
						if (!locationChanged) {
							locationChanged = true;
							mySpot = loc;
							double locy = loc.getLatitude();
							double locx = loc.getLongitude();
							Point wgspoint = new Point(locx, locy);
							Point mapPoint = (Point) GeometryEngine
									.project(wgspoint,
											SpatialReference.create(4326),
											mMapView.getSpatialReference());

							Unit mapUnit = mMapView.getSpatialReference()
									.getUnit();
							double zoomWidth = Unit.convertUnits(
									ZOOM_RADIUS,
									Unit.create(LinearUnit.Code.MILE_US),
									mapUnit);
							Envelope zoomExtent = new Envelope(mapPoint,
									zoomWidth, zoomWidth);
							mMapView.setExtent(zoomExtent);

						}

					}

					public void onProviderDisabled(String arg0) {

					}

					public void onProviderEnabled(String arg0) {
					}

					public void onStatusChanged(String arg0, int arg1,
							Bundle arg2) {

					}
				
});
				ls.start();

			}

		}
	});
 		
    }
    
    // create the action menu from a menu resource XML file
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
       super.onCreateOptionsMenu(menu);
       MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.map_menu, menu);
       return true;
    } // end method onCreateOptionsMenu
    
    // handle choice from options menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
 	   switch (item.getItemId()) 
 	   {
 	   case R.id.tag:
 		   // create a new Intent to launch the AddEditContact Activity
 		  FeatureTemplate template = featureLayer.getTemplates()[0];
			
			//Create a graphic using the template
			Graphic g = featureLayer.createFeatureWithTemplate(template, 
					mMapView.toMapPoint(new Point(mySpot.getLatitude(), mySpot.getLongitude())));
			
			//Send the graphic and symbol up to the server
			featureLayer.applyEdits(new Graphic[] { g }, null, null, 
					new CallbackListener<FeatureEditResult[][]>(){
						
						public void onError(Throwable error) {
							//TODO							
						}
					
						public void onCallback(FeatureEditResult[][] editResult) {
							//TODO  figure out this logic
						}
			});

			//Display a toast telling the user what has been done
			Toast toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
//			myListener.setType("POINT");
			toast.setText("Your current GPS location has been tagged on the map."); 						
			toast.show();											
 		   return true;
 		   
 	   case R.id.startCam:
 		   	Intent cameraIntent = new Intent (android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
 			startActivity(cameraIntent);
 		   
 		   return true;
 		   
 	   default:
 		  return super.onOptionsItemSelected(item); // call super's method
    
 	   }
    
    } // end method onOptionsItemSelected

    
	@Override 
	protected void onDestroy() { 
		super.onDestroy();
 }
	@Override
	protected void onPause() {
		super.onPause();
		mMapView.pause();
 }
	@Override 	protected void onResume() {
		super.onResume(); 
		mMapView.unpause();
	}

}